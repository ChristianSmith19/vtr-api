FROM continuumio/miniconda3:4.9.2-alpine
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_ENV=development
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN conda install --file requirements.txt -y
EXPOSE 5000
COPY . .
CMD ["flask", "run"]