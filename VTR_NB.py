
# coding: utf-8

# # Análisis de Datos VTR
# 
# # Aplicación de probabilidad de Ocurrencia
# 
# # Hipótesis:
# Dado los antecedentes entregados y sabiendo que existe información de afectación de los servicios de WIFI, nos planteamos la siguiente pregunta de hipótesis:
# 
# Debemos actualizar los Access point “WIFI” de los clientes domiciliarios de la compañía?
# 
# Para responder a la pregunta hipótesis realizaremos un análisis de los datos utilizando inteligencia artificial.
# 
# # Nuestros Datos de Entrada:
# 
# TENEMOS EN EL SET DE DATOS:
# - cable_modems : Mac del Device de los clientes domiciliarios.
# - country      : País del Device. (1=CHILE, 2=PANAMA, 3=PUERTO RICO)
# - day          : Día de la muestra.
# - WiFi_Channels: (si=1/no=0) flag que indica problemas en canales wifi
# - sep_ssids    : (si=1/no=0) flag que dice si el access point para ese día, tuvo las bandas separadas.
# - Config       : (si=1/no=0) flag de problemas de configuraciones.
# - low_upstream : (si=1/no=0) flag para decir si tienes bajo upstream o no.
# 
# Además de la serie de datos promedios de throughput, latencia, y rssi
# RSSIavg
# LatAVG
# throughpAVG
# 
# Tomaremos los datos descriptivos que indican si existe una condición especial para ese Device.
# 
# # Algunos supuestos para el problema formulado:
# 
# Se actualizo masivamente el fimware de los access point de wify de 3 Países (CHILE, PUERTO RICO, PANAMA)
# No tiene en cuenta ubicación geográfica de los acces Ponit, por cuanto no sabemos el detalle por las distintas zonas.
# 
# Con esta información, queremos que el algoritmo aprenda y que como resultado podamos consultar nueva información y nos dé la respuesta sobre actualizar (1) o no actualizar (0) masivamente.
# 
# # El teorema de Bayes
# El teorema de Bayes es una ecuación que describe la relación de probabilidades condicionales de cantidades estadísticas. En clasificación bayesiana estamos interesados en encontrar la probabilidad de que ocurra una “clase” dadas unas características observadas (datos). 
# Lo podemos escribir como P( Clase | Datos). 
# 
# El teorema de Bayes nos dice cómo lo podemos expresar en términos de cantidades que podemos calcular directamente:
# ![image.png](attachment:image.png)
# 
# Clase es una salida en particular, por ejemplo “actualizar”
# 
# Datos son nuestras características, en nuestro caso los problemas en wifi, bandas separadas, problemas de configuracion,bajo upstream, etc
# 
# - P(Clase|Datos) se llama posterior (y es el resultado que queremos hallar)
# - P(Datos|Clase) se llama “verosimilitud” (en inglés likelihood)
# - P(Clase) se llama anterior (pues es una probabilidad que ya tenemos)
# - P(Datos) se llama probabilidad marginal

# In[1]:


# Import required libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import seaborn as sb
from IPython import get_ipython

get_ipython().run_line_magic('matplotlib', 'inline')
plt.rcParams['figure.figsize'] = (16, 9)
plt.style.use('ggplot')

from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.naive_bayes import GaussianNB
from sklearn.feature_selection import SelectKBest



def dataProcess(csv_file):
     # In[4]:


     # llama a archivo
     # dataframe = pd.read_csv("C:\\C3L-LAB\\Proyectos\\VTR\DATOS_VOL\\CMsWithIssues20210106-20210112.csv")
     dataframe = pd.read_csv(csv_file)
     dataframe.head()


     # In[5]:


     print(dataframe.groupby('country').size())
     print(dataframe.groupby('WiFi_Channels').size())
     print(dataframe.groupby('sep_ssids').size())
     print(dataframe.groupby('Config').size())
     print(dataframe.groupby('low_upstream').size())


     # # Vamos a preparar los datos.

     # In[6]:


     ## --- Preparamos country
     dataframe.loc[dataframe['country'] == 'CHILE', 'country'] = 1
     dataframe.loc[dataframe['country'] == 'PANAMA', 'country'] = 2
     dataframe.loc[dataframe['country'] == 'PUERTO_RICO', 'country'] = 3


     ## --- Preparamos WiFi_Channels
     dataframe.loc[dataframe['WiFi_Channels'] == 'yes', 'WiFi_Channels'] = 1
     dataframe.loc[dataframe['WiFi_Channels'] == 'no', 'WiFi_Channels'] = 0

     ## --- Preparamos sep_ssids
     dataframe.loc[dataframe['sep_ssids'] == 'yes', 'sep_ssids'] = 1
     dataframe.loc[dataframe['sep_ssids'] == 'no', 'sep_ssids'] = 0

     ## --- Preparamos Config
     dataframe.loc[dataframe['Config'] == 'yes', 'Config'] = 1
     dataframe.loc[dataframe['Config'] == 'no', 'Config'] = 0

     ## --- Preparamos low_upstream
     dataframe.loc[dataframe['low_upstream'] == 'yes', 'low_upstream'] = 1
     dataframe.loc[dataframe['low_upstream'] == 'no', 'low_upstream'] = 0



     # In[7]:


     ## ----- Genera actulizar (si=1 / no=0) 
     actualiza=dataframe['Config']
     dataframe['actualiza']=actualiza



     # In[8]:


     dataframe.head()


     # In[9]:


     print(dataframe.groupby('country').size())
     print(dataframe.groupby('WiFi_Channels').size())
     print(dataframe.groupby('sep_ssids').size())
     print(dataframe.groupby('Config').size())
     print(dataframe.groupby('low_upstream').size())
     print(dataframe.groupby('actualiza').size())


     # In[7]:


     dataframe.drop(['RSSIavg','LatAVG','throughpAVG'], axis=1).hist()
     plt.show()


     # In[10]:


     dataframe.drop(['cable_modems','day'], axis=1).head(10)


     # In[11]:


     reduced = dataframe.drop(['RSSIavg','LatAVG','throughpAVG'], axis=1)
     reduced.describe()


     # In[22]:


     dataframe.drop(['cable_modems','day','RSSIavg','LatAVG','throughpAVG'], axis=1)
     dataframe.head()


     # In[12]:


     dataframe=dataframe.drop(['cable_modems','day','RSSIavg','LatAVG','throughpAVG'], axis=1)


     # In[13]:


     dataframe.head()


     # In[14]:


     X=dataframe.drop(['actualiza'], axis=1)
     y=dataframe['actualiza']
     
     best=SelectKBest(k=3)
     X_new = best.fit_transform(X, y)
     X_new.shape
     selected = best.get_support(indices=True)
     print(X.columns[selected])


     # In[15]:


     used_features =X.columns[selected]
     
     colormap = plt.cm.viridis
     plt.figure(figsize=(12,12))
     plt.title('Pearson Correlation of Features', y=1.05, size=15)
     sb.heatmap(dataframe[used_features].astype(float).corr(),linewidths=0.1,vmax=1.0, square=True, cmap=colormap, linecolor='white', annot=True)


     # # Crear el modelo Gaussian Naive Bayes con SKLearn

     # In[16]:


     # Split dataset in training and test datasets
     X_train, X_test = train_test_split(dataframe, test_size=0.2, random_state=6) 
     y_train =X_train["actualiza"]
     y_test = X_test["actualiza"]


     # In[17]:


     # Instantiate the classifier
     gnb = GaussianNB()
     # Train classifier
     gnb.fit(
     X_train[used_features].values,
     y_train
     )
     y_pred = gnb.predict(X_test[used_features])
     
     print('Precisión en el set de Entrenamiento: {:.2f}'
          .format(gnb.score(X_train[used_features], y_train)))
     print('Precisión en el set de Test: {:.2f}'
          .format(gnb.score(X_test[used_features], y_test)))


     # In[18]:


     #['WiFi_Channels','sep_ssids','Config']
     print(gnb.predict([[1,        1,     1],
                    [0,        0,    1] ]))
     #Resultado esperado 0-no actualiza, 1-actualiza

