import time

import csv
import codecs
from flask import Flask
from flask import (jsonify, request)

app = Flask(__name__)

@app.route('/')
def test():
    return {'message': 'Server Running!'}

@app.route('/csv-reader', methods=['POST'])
def read_CSV():
    flask_file = request.files['file']
    if not flask_file:
        return 'Upload a CSV file'
    
    data = []
    stream = codecs.iterdecode(flask_file.stream, 'utf-8')
    for row in csv.reader(stream, dialect=csv.excel):
        if row:
            data.append(row)
            
    return jsonify(data)